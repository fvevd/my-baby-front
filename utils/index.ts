export function plural(count: number, one: string, few: string, many: string): string {
  const i = Math.floor(Math.abs(count));
  const v = count.toString().replace(/^[^.]*\.?/, '').length;

  // if float
  if (v !== 0) {
    return '-';
  }

  if (
    // ends with 1
    i % 10 === 1 &&
    // but not 11
    !(i % 100 === 11)
  ) {
    return one;
  }

  if (
    // ends with 2, 3, 4
    i % 10 >= 2 && i % 10 <= 4 &&
    // but not 12, 13, 14
    !(i % 100 >= 12 && i % 100 <= 14)
  ) {
    return few;
  }

  if (
    // ends with 0
    i % 10 === 0 ||
    // ends with 5, 6, 7, 8, 9
    (i % 10 >= 5 && i % 10 <= 9) ||
    // ends with 11, 12, 13, 14
    (i % 100 >= 11 && i % 100 <= 14)
  ) {
    return many;
  }

  return '-';
}

export function price(sum: number): string {
  const int = Math.floor(sum);
  const float = Math.round((sum - int) * 100);

  return `${int}.${float || '–'}`;
}

const entities: { [encoded: string]: string } = {
  'amp': '&',
  'apos': '\'',
  '#x27': '\'',
  '#x2F': '/',
  '#39': '\'',
  '#47': '/',
  'lt': '<',
  'gt': '>',
  'nbsp': ' ',
  'quot': '"',
};

export function decodeHTMLEntities(text: string) {
  if (!text) {
    return '';
  }

  return text.replace(
    /&([^;]+);/gm,
    (match, entity) => entities[entity] || match,
  );
}
