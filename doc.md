### MyBaby

## Components

* Header
  * menu with burger
  * location selector
  * favorites
  * cart
  * callback
* Floating Footer
  * menu
  * favorites
  * cart
  * callback
* Location Selector
  * popup
  * search input with clear button
  * Tooltip
* Item Card
  * image
  * action buttons
* Advanced Filter
  * floating container
  * range input with steps
  * item counter
  * popup
* Callback
  * input with mask
  * button
  * tooltip

## Pages

* main with catalog
  * links to anchors / filter
  * item card
  * advanced filter
* installments (rassrochka)
* payment and delivery
* my orders
  * input
  * button
  * orders list
  * close button
  * order card
* item details
  * image
  * image selector
  * dimensions
  * floating top menu
  * floating footer
* compare items
  * columns
  * scroll column buttons
  * filter
  * action buttons
  * floating top menu
* cart
* checkout
  * input with mask & tooltip
  * button
  * circular modal with success
