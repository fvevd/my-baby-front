module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  // parserOptions: {
  //   parser: 'babel-eslint',
  //   ecmaFeatures: {
  //     legacyDecorators: true,
  //   },
  // },
  extends: [
    '@nuxtjs',
    'plugin:nuxt/recommended',
    '@nuxtjs/eslint-config-typescript',
  ],
  // add your custom rules here
  rules: {
    'comma-dangle': ['error', 'always-multiline'],
    semi: ['error', 'always'],
    'space-before-function-paren': ['error', 'never'],
    '@typescript-eslint/no-unused-vars': 'warn',
    'no-unused-vars': 'warn',
    'no-useless-constructor': 'off',
    'require-await': 'off',
    'arrow-parens': ['error', 'as-needed'],
    'quote-props': ['error', 'consistent-as-needed'],
    'vue/no-v-html': 'off',
  },
}
