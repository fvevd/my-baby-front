FROM node:14-slim as base

WORKDIR /opt/app

RUN groupadd -g 999 appuser && \
    useradd -m -r -u 999 -g appuser appuser && \
    chown -R appuser:appuser /opt/app

####################

FROM base as builder

USER appuser

COPY package.json yarn.lock ./

RUN yarn

ENV NODE_ENV=production

COPY . .

RUN yarn build

####################

FROM base

COPY --from=builder --chown=appuser:appuser /opt/app/.nuxt /opt/app/.nuxt
COPY --from=builder --chown=appuser:appuser /opt/app/node_modules /opt/app/node_modules
COPY --from=builder --chown=appuser:appuser /opt/app/static /opt/app/static
COPY --from=builder --chown=appuser:appuser /opt/app/nuxt.config.js /opt/app/package.json /opt/app/tsconfig.json /opt/app/

USER appuser

ENV NODE_ENV=production

ENTRYPOINT yarn start
