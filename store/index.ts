import Vue from 'vue';
import { Store } from 'vuex';
import {
  Actions,
  createMapper,
  createStore,
  Getters,
  Module,
  Mutations,
} from 'vuex-smart-module';

import { TItem, TItemMap } from '~/types/item';
import { TLocation } from '~/types/location';
import { TFilter } from '~/types/filter';
import { TUser } from '~/types/user';
import { TOrder } from '~/types/order';
import { TContent } from '~/types/content';
import {
  TCategory,
  TCategoryItems,
  TCategoryMap,
  TCategoryParentMap,
  TCategoryTree,
} from '~/types/category';

import { Api, initApi } from '~/api';

const LOCATION_KEY = 'location';

class MainState {
  items: TItem[] = [];
  categories: TCategory[] = [];
  categoryMap: TCategoryMap = {};
  locations: TLocation[] = [];

  filter: TFilter = {};
  wishList: string[] = [];
  cart: string[] = [];
  user: TUser = {};
  orders: TOrder[] = [];
  callbackPhone: string = '';

  content: TContent = {};

  showCart: boolean = false;
  detectedLocation: boolean = false;
}

class MainGetters extends Getters<MainState> {
  get filterableItems() {
    return this.state.items.filter(item => !item.isSand);
  }

  get filteredItems() {
    const { price, category, area } = this.state.filter;

    return this.getters.filterableItems.filter(item => {
      if (price?.from && price?.to) {
        if (item.price < price.from || item.price > price.to) {
          return false;
        }
      }

      if (category) {
        return item.categories
          .some(id => this.getters.categoryParents[id].includes(category));
      }

      if (area?.from || area?.to) {
        if (!item.dimensions?.area) {
          return true;
        }

        return item.dimensions.area >= (area.from ?? 0) &&
          (!area.to || item.dimensions.area < area.to);
      }

      return true;
    });
  }

  get filteredCategoryItems(): TCategoryItems {
    const { category: filterCategory } = this.state.filter;
    const items = this.getters.filteredItems.map(item => ({
      id: item.id,
      categories: item.categories,
    }));

    const categoryMap = items.reduce((map, { id, categories }) => {
      categories.forEach(category => {
        if (
          filterCategory &&
          !this.getters.categoryParents[category].includes(filterCategory)
        ) {
          return;
        }

        if (!map[category]) {
          map[category] = [];
        }

        map[category].push(id);
      });

      return map;
    }, {} as { [categoryId: string]: string[] });

    Object.values(categoryMap).forEach(ids => {
      ids.sort((a, b) => (
        this.getters.itemsById[a].sort - this.getters.itemsById[b].sort
      ));
    });

    const categories = Object.keys(this.state.categoryMap);
    categories.sort((a, b) => {
      const aParents = this.getters.categoryParents[a];
      const bParents = this.getters.categoryParents[b];

      if (aParents === bParents) {
        return 0;
      }

      return aParents > bParents ? 1 : -1;
    });

    return categories
      .filter(id => categoryMap[id]?.length)
      .map(id => ({
        id,
        name: this.state.categoryMap[id].name,
        items: categoryMap[id].map(itemId => this.getters.itemsById[itemId]),
      }));
  }

  get sandItems() {
    return this.state.items.filter(item => item.isSand);
  }

  get wishListBadge() {
    // there are might be same items in different categories
    const ids = new Set();

    this.getters.wishListCategoryItems.forEach(category => {
      category.items.forEach(item => {
        ids.add(item.id);
      });
    });

    return ids.size || (this.state.wishList.length ? 1 : 0);
  }

  get wishListItems(): TItem[] {
    return this.state.wishList.map(id => this.getters.itemsById[id]);
  }

  get cartItems(): TItem[] {
    return this.state.cart.map(id => this.getters.itemsById[id]);
  }

  get cartTotal() {
    return this.getters.cartItems.reduce((sum, item) => {
      sum += item.price ?? 0;
      return sum;
    }, 0);
  }

  get itemsByUrl(): TItemMap {
    return this.state.items.reduce((map, item) => {
      map[item.url] = item;
      return map;
    }, {} as TItemMap);
  }

  get itemsById(): TItemMap {
    return this.state.items.reduce((map, item) => {
      map[item.id] = item;
      return map;
    }, {} as TItemMap);
  }

  get categoryTree(): TCategoryTree[] {
    const walk = (category: TCategory): TCategoryTree => {
      let children: TCategoryTree[] = [];

      if (category.children.length) {
        children = category.children.map(id => walk(this.state.categoryMap[id]));
      }

      return {
        id: category.id,
        name: category.name,
        children,
      };
    };

    return this.state.categories.map(walk);
  }

  get categoryParents(): TCategoryParentMap {
    const map: TCategoryParentMap = {};
    const walk = (category: TCategoryTree, parents: string[] = []) => {
      if (category.children.length) {
        category.children.forEach(child => {
          walk(child, [...parents, category.id]);
        });
      }

      map[category.id] = [...parents, category.id];
    };

    this.getters.categoryTree.forEach(category => {
      walk(category);
    });

    return map;
  }

  get wishListCategoryItems(): TCategoryItems {
    const items = this.getters.wishListItems.map(item => ({
      id: item.id,
      categories: item.categories,
    }));

    const categoryMap = items.reduce((map, { id, categories }) => {
      categories.forEach(category => {
        if (!map[category]) {
          map[category] = [];
        }

        map[category].push(id);
      });

      return map;
    }, {} as { [categoryId: string]: string[] });

    Object.values(categoryMap).forEach(ids => {
      ids.sort((a, b) => (
        this.getters.itemsById[a].sort - this.getters.itemsById[b].sort
      ));
    });

    const categories = Object.keys(this.state.categoryMap);
    categories.sort((a, b) => {
      const aParents = this.getters.categoryParents[a];
      const bParents = this.getters.categoryParents[b];

      if (aParents === bParents) {
        return 0;
      }

      return aParents > bParents ? 1 : -1;
    });

    return categories
      .filter(id => categoryMap[id]?.length > 1)
      .map(id => ({
        id,
        name: this.state.categoryMap[id].name,
        items: categoryMap[id].map(itemId => this.getters.itemsById[itemId]),
      }));
  }

  get citiesMap() {
    return this.state.locations.reduce((map, loc) => {
      map[loc.name] = loc;
      return map;
    }, {} as { [name: string]: TLocation });
  }
}

class MainMutations extends Mutations<MainState> {
  setItems(items: TItem[]) {
    Vue.set(this.state, 'items', items);

    let minPrice: number | undefined;
    let maxPrice: number | undefined;
    let minArea: number | undefined;
    let maxArea: number | undefined;

    items.forEach(({ isSand, price, dimensions }) => {
      if (isSand) {
        return;
      }

      minPrice = minPrice ? Math.min(minPrice, price) : price;
      maxPrice = maxPrice ? Math.max(maxPrice, price) : price;

      if (dimensions?.area) {
        minArea = minArea ? Math.min(minArea, dimensions.area) : dimensions.area;
        maxArea = maxArea ? Math.max(maxArea, dimensions.area) : dimensions.area;
      }
    });

    if (minPrice) {
      minPrice = Math.floor(minPrice / 1000) * 1000;
    }
    if (maxPrice) {
      maxPrice = Math.ceil(maxPrice / 1000) * 1000;
    }

    minPrice = minPrice ?? 1000;
    maxPrice = maxPrice ?? 100000;

    Vue.set(this.state.filter, 'price', {
      min: minPrice,
      max: maxPrice,
      from: minPrice,
      to: maxPrice,
    } as TFilter['price']);

    Vue.set(this.state.filter, 'area', {
      min: minArea,
      max: maxArea,
    } as TFilter['area']);
  }

  setCategories({
    categories,
    categoryMap,
  }: {
    categories: TCategory[],
    categoryMap: TCategoryMap
  }) {
    this.state.categories = categories;
    this.state.categoryMap = categoryMap;
  }

  setLocations(locations: TLocation[]) {
    Vue.set(this.state, 'locations', locations);
  }

  setShowCart(value: boolean) {
    Vue.set(this.state, 'showCart', value);
  }

  setFilter(filter: TFilter) {
    Vue.set(this.state, 'filter', filter);
  }

  addToWishList(id: string) {
    if (!this.state.wishList.includes(id)) {
      this.state.wishList.push(id);
    }
  }

  removeFromWishList(id: string) {
    const index = this.state.wishList.indexOf(id);

    if (index === -1) {
      return;
    }

    this.state.wishList.splice(index, 1);
  }

  addToCart(id: string) {
    this.state.cart.push(id);
  }

  removeFromCart(index: number) {
    if (index > this.state.cart.length - 1 || index < 0) {
      return;
    }

    this.state.cart.splice(index, 1);
  }

  clearCart() {
    this.state.cart = [];
  }

  setUser(user: TUser) {
    this.state.user = user;

    if (user?.city) {
      localStorage.setItem(LOCATION_KEY, user.city);
    } else {
      localStorage.removeItem(LOCATION_KEY);
    }
  }

  setOrders(orders: TOrder[]) {
    this.state.orders = orders;
  }

  addOrder(order: TOrder) {
    if (this.state.orders.some(ord => ord.id === order.id)) {
      return;
    }

    this.state.orders.push(order);
  }

  setContent({
    key,
    content,
  }: {
    key: keyof TContent,
    content: string,
  }) {
    this.state.content[key] = content;
  }

  setDetectedLocation(detectedLocation: boolean) {
    this.state.detectedLocation = detectedLocation;
  }

  setCallbackPhone(callbackPhone: string) {
    this.state.callbackPhone = callbackPhone;
  }
}

class MainActions extends Actions<MainState, MainGetters, MainMutations, MainActions> {
  private api!: Api;

  $init(store: Store<any>): void {
    this.api = initApi(store);
  }

  async getLocation() {
    const { locations } = this.state;
    let location: string | undefined = localStorage.getItem(LOCATION_KEY) || undefined;
    if (location && locations.some(loc => loc.name === location)) {
      this.mutations.setDetectedLocation(false);

      return location;
    }

    const coords = await new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(
        ({ coords }) => resolve(coords),
        error => reject(error),
      );
    }).catch(e => {
      // fixme
      console.error('geolocation error:', e);
      return undefined;
    }) as Coordinates | undefined;

    if (coords?.latitude && coords?.longitude) {
      location = await this.api.getLocation(coords.latitude, coords.longitude);
      if (location && locations.some(loc => loc.name === location)) {
        this.mutations.setDetectedLocation(true);

        return location;
      }
    }

    this.mutations.setDetectedLocation(true);

    return 'Новосибирск';
  }

  async initLocation() {
    const location: string = await this.dispatch('getLocation');

    this.mutations.setUser({
      ...this.state.user,
      city: location,
    });
  }

  async loadItems() {
    const items = await this.api.getItems();

    this.mutations.setItems(items);

    this.mutations.addToWishList(items[0].id);
    // this.mutations.addToWishList(items[1].id);
    // this.mutations.addToWishList(items[2].id);

    // this.mutations.setOrders([
    //   {
    //     id: 'H3n-94R',
    //     created: new Date('2019-11-18 21:32'),
    //     status: EOrderStatus.new,
    //     user: {
    //       name: 'Сарафанова С. С.',
    //       phone: '+7 983 ••• ••18',
    //       email: 'sar•••••••@•••••••ova.ru',
    //       city: 'Новосибирск',
    //       pickup: true,
    //     },
    //     items: [
    //       items[0],
    //       items[1],
    //     ],
    //   },
    //   {
    //     id: 'Qp8-3m7',
    //     created: new Date('2019-12-11 21:32'),
    //     status: EOrderStatus.paid,
    //     user: {
    //       name: 'Сарафанова С. С.',
    //       phone: '+7 983 ••• ••18',
    //       email: 'sar•••••••@•••••••ova.ru',
    //       city: 'Новосибирск',
    //       pickup: false,
    //       address: 'ул. Саратовская 2',
    //     },
    //     items: [
    //       items[0],
    //       items[1],
    //     ],
    //   },
    // ]);
  }

  async loadCategories() {
    const { categories, map } = await this.api.getCategories();

    this.mutations.setCategories({
      categories,
      categoryMap: map,
    });
  }

  async loadLocations() {
    const locations = await this.api.getLocations();

    this.mutations.setLocations(locations);
  }

  async loadContent({
    key,
  }: {
    key: keyof TContent,
  }): Promise<string> {
    let content;

    content = this.state.content[key];

    if (content) {
      return content;
    }

    switch (key) {
      case 'delivery':
        content = await this.api.getDeliveryContent();
      // case 'installments':
    }

    if (content) {
      this.mutations.setContent({
        key,
        content,
      });
    }

    return content ?? '';
  }

  async openCart() {
    this.mutations.setShowCart(true);
  }

  async closeCart() {
    this.mutations.setShowCart(false);
  }

  async changePriceFilter({
    from,
    to,
  }: {
    from: number,
    to: number,
  }) {
    const { min, max } = this.state.filter.price ?? {};

    if (!min || !max) {
      return;
    }

    this.mutations.setFilter({
      ...this.state.filter,
      price: {
        min,
        max,
        from: Math.max(min, from),
        to: Math.min(max, to),
      },
    });
  }

  async changeCategoryFilter({
    category,
  }: {
    category?: string,
  }) {
    this.mutations.setFilter({
      ...this.state.filter,
      category,
    });
  }

  async changeAreaFilter({
    from,
    to,
  }: {
    from?: number,
    to?: number,
  }) {
    this.mutations.setFilter({
      ...this.state.filter,
      area: {
        ...this.state.filter.area,
        from,
        to,
      },
    });
  }

  addToWishList({
    id,
  }: {
    id: string,
  }) {
    this.mutations.addToWishList(id);
  }

  removeFromWishList({
    id,
  }: {
    id: string,
  }) {
    this.mutations.removeFromWishList(id);
  }

  addToCart({
    id,
  }: {
    id: string,
  }) {
    this.mutations.addToCart(id);
  }

  removeFromCart({
    index,
  }: {
    index: number,
  }) {
    this.mutations.removeFromCart(index);
  }

  removeFromCartById({
    id,
  }: {
    id: string,
  }) {
    const indexes = this.state.cart.reduce((arr, _id, index) => {
      if (_id === id) {
        arr.push(index);
      }
      return arr;
    }, [] as number[]);

    indexes.forEach(index => this.actions.removeFromCart({ index }));
  }

  updateUser({
    user,
  }: {
    user: TUser,
  }) {
    this.mutations.setUser(user);
  }

  async placeOrder({
    user,
    items,
  }: {
    user: TUser,
    items: TItem[],
  }) {
    const order = await this.api.createOrder({ user, items });

    this.mutations.addOrder(order);
    this.mutations.clearCart();
  }

  async confirmLocation() {
    this.mutations.setDetectedLocation(false);
  }

  async setCallbackPhone({
    callbackPhone,
  }: {
    callbackPhone: string,
  }) {
    this.mutations.setCallbackPhone(callbackPhone);
  }

  async nuxtServerInit() {
    await Promise.all([
      this.actions.loadItems(),
      this.actions.loadCategories(),
      this.actions.loadLocations(),
      this.actions.loadContent({ key: 'delivery' }),
    ]);
  }

  async nuxtClientInit() {
    await Promise.all([
      this.actions.initLocation(),
    ]);
  }
}

const main = new Module({
  state: MainState,
  getters: MainGetters,
  mutations: MainMutations,
  actions: MainActions,
});

export default () => createStore(main, {
  strict: true,
});

export const initStore = (st: Store<any>) => main.context(st);

export const mapper = createMapper(main);
