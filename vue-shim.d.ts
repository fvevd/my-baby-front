declare module '*.vue' {
  import Vue from 'vue';
  export default Vue;
}

declare module '*.svg?inline' {
  import Vue from 'vue';
  export default Vue;
}

declare module 'vue-slider-component/dist-css/vue-slider-component.umd.min.js' {
  import VueSlider from 'vue-slider-component';
  export default VueSlider;
}

declare module 'vue-click-outside' {
  import Vue, { DirectiveOptions } from 'vue';

  const vClickOutside: DirectiveOptions;
  export default vClickOutside;
}
