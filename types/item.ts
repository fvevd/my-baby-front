export type TItem = {
  id: string;
  url: string;
  shortTitle?: string;
  title: string;
  description?: string;
  price: number;
  image: string;
  images: Array<{
    image: string;
    thumb: string;
  }>;
  equipment?: string;
  dimensions?: {
    width: number;
    height: number;
    length: number;
    custom: number;
    customName: string;
    text: string;
    area?: number;
  };
  warranty?: {
    duration: number;
    all: string;
    includes: string;
  };
  note?: string;
  type: any;
  haveInstallments: boolean;
  isSand: boolean;
  sort: number;
  categories: string[];
  relatedItems?: string[];
};

export type TItemMap = {
  [key: string]: TItem;
};
