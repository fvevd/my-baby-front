export type TFilter = {
  price?: {
    min: number;
    max: number;
    from: number;
    to: number;
  };
  category?: string;
  area?: {
    min?: number;
    max?: number;
    from?: number;
    to?: number;
  };
};
