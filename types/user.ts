export type TUser = {
  name?: string;
  phone?: string;
  email?: string;
  pickup?: boolean;
  city?: string; // id?
  address?: string;
};
