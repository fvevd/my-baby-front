import { TUser } from '~/types/user';
import { TItem } from '~/types/item';

export enum EOrderStatus {
  new = 'new',
  paid = 'paid',
}

export type TOrder = {
  id: string;
  created: Date;
  status: EOrderStatus;
  user: TUser;
  items: TItem[];
}
