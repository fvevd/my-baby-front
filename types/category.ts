import { TItem } from '~/types/item';

export type TCategory = {
  id: string;
  name: string;
  children: string[];
};

export type TCategoryMap = {
  [id: string]: TCategory;
};

export type TCategoryTree = Omit<TCategory, 'children'> & {
  children: TCategoryTree[];
};

export type TCategoryParentMap = {
  [id: string]: string[];
};

export type TCategoryItems = Array<Omit<TCategory, 'children'> & {
  items: TItem[],
}>;
