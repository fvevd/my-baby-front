export type TLocation = {
  name: string;
  office: string;
  isPrior?: boolean;
};
