import { Store } from 'vuex';

import { TItem } from '~/types/item';
import { TLocation } from '~/types/location';
import { EOrderStatus, TOrder } from '~/types/order';
import { TUser } from '~/types/user';
import { TCategory, TCategoryMap } from '~/types/category';

import { decodeHTMLEntities } from '~/utils';

/* eslint-disable camelcase */
type TContentResponse = {
  breadcrumbs: Array<{
    text: string;
    href: string;
  }>;
  heading_title: string;
  description: string;
  continue: string;
  column_left: string;
  column_right: string;
  content_top: string;
  content_bottom: string;
  footer: string;
  header: string;
};

type TProductsResponse = {
  [productId: number]: {
    product_id: number;
    name: string;
    description: string;
    meta_title: string;
    meta_description: string;
    meta_keyword: string;
    tag: string;
    model: string;
    sku: string;
    upc: string;
    extra_price: string; // "0.00"
    equipment: string;
    footnotes: string;
    custom_attribute_name: string;
    custom_attribute_value: string; // "3"
    dimension_text: string;
    ean: string;
    jan: string;
    isbn: string;
    mpn: string;
    location: string;
    quantity: string;
    stock_status: string; // "В наличии" enum?
    image: string;
    manufacturer_id: null;
    manufacturer: null;
    price: string;
    special: boolean;
    reward: null;
    points: string; // "0"
    tax_class_id: string; // "0"
    date_available: string; // "2020-04-15"
    weight: string; // "0.00"
    weight_class_id: string; // "1"
    length: string; // "1.20"
    width: string; // "1.20"
    height: string; // "1.50"
    length_class_id: string; // "1"
    subtract: string; // "1"
    rating: number;
    reviews: number;
    minimum: string; // "1" min amount?
    sort_order: string; // "1"
    status: string; // "1" enum?
    date_added: string; // "2020-04-15 17:05:05"
    date_modified: string; // "2020-04-28 16:01:05"
    viewed: string; // "0"
    heading_title: string;
    manufacturers: string;
    stock: string; // "text_instock" enum?
    popup: string; // 500x500 pic
    thumb: string; // 228x228 pic
    images: Array<{
      popup: string | null; // 500x500 pic
      thumb: string | null; // 74x74 pic
    }>;
    share: string;
    product_varranty: {
      id: string; // "4"
      product_id: string; // "50"
      years: string; // "6"
      text: string;
      long_text: string;
    } | [];
    related_products: Array<{
      product_id: string; // "53"
      thumb: string; // 200x200 pic
      name: string;
      description: string;
      price: string; // "73030р."
      special: boolean;
      tax: boolean;
      minimum: string; // "1"
      rating: number;
      href: string;
    }>;
    categories: string[]; // "62"
  };
};

type TSitemapCategory = {
  name: string;
  href: string;
  children: TSitemapCategory[];
};

type TSitemapResponse = {
  breadcrumbs: Array<{
    text: string;
    href: string;
  }>;
  categories: TSitemapCategory[];
};

type TOSMResponse = {
  place_id: string;
  licence: string;
  osm_type: 'way' | 'node' | 'relation';
  osm_id: string;
  lat: string;
  lon: string;
  place_rank: string;
  category: string;
  type: string;
  importance: string;
  addresstype: string;
  display_name: string;
  name: string;
  address: {
    country: string;
    country_code: string; // 2 lowercase letters
    road?: string;
    village?: string;
    state?: string;
    state_district?: string;
    postcode?: string;
    city?: string;
    region?: string;
  };
  boundingbox: [string, string, string, string];
};
/* eslint-enable camelcase */

const dummyLocation = (
  name: string,
  isPrior?: boolean,
): TLocation => ({
  name,
  office: 'ул. Ленина, оф. 1',
  isPrior,
});

export class Api {
  private readonly apiUrl = process.env.apiHost;
  private readonly osmUrl = 'https://nominatim.openstreetmap.org/reverse';

  constructor(
    private readonly ctx: Store<any>,
  ) {}

  private async makeRequest(
    method: 'GET' | 'POST',
    route: string,
    data?: { [key: string]: any; },
  ) {
    const url = `index.php?route=${route}`;

    const params: { [key: string]: any; } = method === 'GET' ? data || {} : {};

    return this.ctx.$axios
      .request({
        method,
        url,
        baseURL: this.apiUrl,
        params,
        data,
        timeout: 15000,
      })
      .then(({ data }) => data);
  }

  async getItems(): Promise<TItem[]> {
    const products: TProductsResponse = await this.makeRequest(
      'GET',
      'product/category/products',
    );

    if (!products || typeof products !== 'object' || !Object.keys(products).length) {
      return [];
    }

    return Object.values(products).map((product, i) => ({
      id: String(product.product_id),
      url: `item_${product.product_id}`,
      shortTitle: product.model,
      title: product.name + product.model,
      description: product.description,
      price: parseFloat(product.price),
      image: product.images.find(image => image.popup !== null)?.popup || product.image || '',
      images: product.images.filter(image => image.popup !== null).map(image => ({ image: image.popup, thumb: image.thumb })) as TItem['images'],
      equipment: decodeHTMLEntities(product.equipment),
      dimensions: parseFloat(product.width) > 0 ? {
        width: parseFloat(product.width) * 1000,
        height: parseFloat(product.height) * 1000,
        length: parseFloat(product.length) * 1000,
        custom: parseFloat(product.custom_attribute_value) * 1000,
        customName: product.custom_attribute_name,
        text: product.dimension_text,
        area: parseFloat(product.width) * parseFloat(product.length),
      } : undefined,
      warranty: Array.isArray(product.product_varranty) ? undefined : {
        duration: parseInt(product.product_varranty.years),
        includes: product.product_varranty.text,
        all: product.product_varranty.long_text,
      },
      note: decodeHTMLEntities(product.footnotes),
      type: null, // fixme
      haveInstallments: false,
      isSand: false,
      sort: parseInt(product.sort_order),
      categories: product.categories,
      relatedItems: product.related_products?.length
        ? product.related_products.map(related => related.product_id)
        : undefined,
    }));
  }

  async getLocations(): Promise<TLocation[]> {
    return [
      dummyLocation('Москва', true),
      dummyLocation('Санкт-Петербург', true),
      dummyLocation('Омск', true),
      dummyLocation('Новосибирск', true),
      dummyLocation('Красноярск', true),
      dummyLocation('Ярославль', true),
      dummyLocation('Тула', true),
      dummyLocation('Брянск', true),
      dummyLocation('Екатеринбург', true),
      dummyLocation('Челябинск', true),
      dummyLocation('Нижний', true),
      dummyLocation('Новгород', true),
      dummyLocation('Краснодар', true),
      dummyLocation('Ростов-на-Дону', true),
      dummyLocation('Владивосток', true),
      dummyLocation('Железногорск', true),
      dummyLocation('Казань', true),
      dummyLocation('Сочи', true),

      dummyLocation('Абакан'),
      dummyLocation('Аксай'),
      dummyLocation('Алексин'),
      dummyLocation('Алтайский край'),
      dummyLocation('Амурская область'),
      dummyLocation('Анадырь'),
      dummyLocation('Архангельск'),
      dummyLocation('Архангельская область'),
      dummyLocation('Астраханская область'),
      dummyLocation('Астрахань'),

      dummyLocation('Ббакан'),
      dummyLocation('Бксай'),
      dummyLocation('Блексин'),
      dummyLocation('Блтайский край'),
      dummyLocation('Бмурская область'),
      dummyLocation('Бнадырь'),
      dummyLocation('Брхангельск'),
      dummyLocation('Брхангельская область'),
      dummyLocation('Бстраханская область'),
      dummyLocation('Бстрахань'),

      dummyLocation('Вбакан'),
      dummyLocation('Вксай'),
      dummyLocation('Влексин'),
      dummyLocation('Влтайский край'),
      dummyLocation('Вмурская область'),
      dummyLocation('Внадырь'),
      dummyLocation('Врхангельск'),
      dummyLocation('Врхангельская область'),
      dummyLocation('Встраханская область'),
      dummyLocation('Встрахань'),

      dummyLocation('Гбакан'),
      dummyLocation('Гксай'),
      dummyLocation('Глексин'),
      dummyLocation('Глтайский край'),
      dummyLocation('Гмурская область'),
      dummyLocation('Гнадырь'),
      dummyLocation('Грхангельск'),
      dummyLocation('Грхангельская область'),
      dummyLocation('Гстраханская область'),
      dummyLocation('Гстрахань'),
    ];
  }

  async createOrder({
    user,
    items,
  }: {
    user: TUser,
    items: TItem[],
  }): Promise<TOrder> {
    let id = '';

    // generate id
    const symbols = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    for (let i = 0; i < 7; ++i) {
      if (i === 3) {
        id += '-';
        continue;
      }

      id += symbols[Math.ceil(Math.random() * symbols.length) - 1];
    }

    return {
      id,
      created: new Date(),
      status: EOrderStatus.new,
      user: JSON.parse(JSON.stringify(user)),
      items: JSON.parse(JSON.stringify(items)),
    };
  }

  async getDeliveryContent() {
    const content: TContentResponse = await this.makeRequest(
      'GET',
      'information/information',
      {
        information_id: 6,
      },
    );

    return content?.description ?? '';
  }

  private processCategories(
    category: TSitemapCategory,
    map: TCategoryMap,
  ): TCategory['id'] {
    const id = category.href.replace(/^.*path=(\d+_)*(\d+)$/, '$2');

    map[id] = {
      id,
      name: category.name,
      children: [],
    };

    if (category.children.length) {
      map[id].children = category.children.map(child => this.processCategories(child, map));
    }

    return id;
  }

  async getCategories(
  ): Promise<{
    categories: TCategory[],
    map: TCategoryMap,
  }> {
    const sitemap: TSitemapResponse = await this.makeRequest(
      'GET',
      'information/sitemap',
    );

    const map: TCategoryMap = {};

    if (!sitemap?.categories?.length) {
      return { categories: [], map: {} };
    }

    const categories = sitemap.categories.map(category => (
      map[this.processCategories(category, map)]
    ));

    return {
      categories,
      map,
    };
  }

  async getLocation(
    lat: number,
    lon: number,
  ) {
    // see https://nominatim.org/release-docs/develop/api/Reverse/
    const response: TOSMResponse = await this.ctx.$axios
      .request({
        method: 'GET',
        url: this.osmUrl,
        params: {
          'format': 'jsonv2',
          'zoom': 10,
          'accept-language': 'ru-RU,ru',
          'addressdetails': 1,
          lat,
          lon,
        },
        timeout: 15000,
      })
      .then(({ data }) => data);

    return response?.address?.city;
  }
}

export function initApi(ctx: Store<any>) {
  return new Api(ctx);
}
