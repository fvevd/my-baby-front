# MyBabyNsk frontend repo

## Description

Клиентская часть построена на фреймворке
Nuxt.js с использованием ssr, vue.js,
typescript.

## Build Setup

``` bash
# install dependencies
$ yarn

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start
```

## Docker

### environment variables

`API_URL` - полный урл (с протоколом) до апи

### ports

Сервер слушает `3000` порт

### volumes

Файлы статики лежат в контейнере по пути
`/opt/app/static`. Можно подключить вольюм
и отдавать их через nginx (должно быть быстрее),
а можно не подключать и тогда они будут
отдаваться нодой.

### examples

Пример запуска докер-контейнера можно
подглядеть в docker-compose.yml
